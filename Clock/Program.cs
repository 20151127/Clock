﻿using System;
using System.Windows.Forms;
using Clock.UI;
using Clock.Utils.MsgBox;

namespace Clock
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            using (MessageBoxCenteringService.Initialize())
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new FormMain());
            }
        }
    }
}