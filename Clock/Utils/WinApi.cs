﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Clock.Utils
{
    public static class WinApi
    {
        public const int WM_SYSCOMMAND = 0x00000112;
        public const int MF_POPUP = 0x00000010;
        public const int MF_CHECKED = 0x00000008;
        public const int MF_UNCHECKED = 0x00000000;
        public const int MF_STRING = 0x00000000;
        public const int MF_SEPARATOR = 0x00000800;
        public const int MF_BYCOMMAND = 0x00000000;
        public const int MF_BYPOSITION = 0x00000400;

        [DllImport("user32", CharSet = CharSet.Auto)]
        public static extern IntPtr CreateMenu();

        [DllImport("user32", CharSet = CharSet.Auto)]
        public static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport("user32", CharSet = CharSet.Auto)]
        public static extern bool DeleteMenu(IntPtr hMenu, uint uPosition, uint uFlags);

        [DllImport("user32", CharSet = CharSet.Auto)]
        public static extern IntPtr AppendMenu(IntPtr hMenu, int uFlags, int uIDNewItem, string lpNewItem);

        [DllImport("user32", CharSet = CharSet.Auto)]
        public static extern IntPtr InsertMenu(IntPtr hMenu, int uPosition, int uFlags, int uIDNewItem, string lpNewItem);

        [DllImport("user32", CharSet = CharSet.Auto)]
        public static extern IntPtr GetMenuString(IntPtr hMenu, int wIDItem, StringBuilder lpString, int nMaxCount, int wFlag);

        [DllImport("user32", CharSet = CharSet.Auto)]
        public static extern IntPtr CheckMenuItem(IntPtr hMenu, int wIDCheckItem, int wCheck);

        [DllImport("user32", CharSet = CharSet.Auto)]
        public static extern IntPtr CheckMenuRadioItem(IntPtr hMenu, int un1, int un2, int un3, int un4);

        [DllImport("user32", CharSet = CharSet.Auto)]
        public static extern int GetMenuState(IntPtr hMenu, int wID, int wFlags);

        [DllImport("user32", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage(IntPtr hwnd, int wMsg, int wParam, IntPtr lParam);
    }
}