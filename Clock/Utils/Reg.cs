﻿using System;
using System.Drawing;
using Microsoft.Win32;

namespace Clock.Utils
{
    public sealed class Reg
    {
        private static Reg _instance = null;
        private const string _key = @"Software\MySoftWare\Clock";

        private Reg()
        {

        }

        public static Reg Instance
        {
            get
            {
                return Reg._instance ?? (Reg._instance = new Reg());
            }
        }

        public string Get(string key)
        {
            try
            {
                using (RegistryKey regkMain = Registry.CurrentUser)
                {
                    using (RegistryKey regk = regkMain.CreateSubKey(Reg._key))
                    {
                        return regk.GetValue(key)?.ToString();
                    }
                }
            }
            catch
            {
                return null;
            }
        }

        public void Set(string key, string value)
        {
            try
            {
                using (RegistryKey regkMain = Registry.CurrentUser)
                {
                    using (RegistryKey regk = regkMain.CreateSubKey(Reg._key))
                    {
                        regk.SetValue(key, value);
                    }
                }
            }
            catch
            {

            }
        }

        public void RegForm(Point point)
        {
            try
            {
                using (RegistryKey regkMain = Registry.CurrentUser)
                {
                    using (RegistryKey regk = regkMain.CreateSubKey(Reg._key))
                    {
                        regk.SetValue("Left", point.X);
                        regk.SetValue("Top", point.Y);
                    }
                }
            }
            catch
            {

            }
        }

        public bool LoadForm(out Point point)
        {
            try
            {
                using (RegistryKey regkMain = Registry.CurrentUser)
                {
                    using (RegistryKey regk = regkMain.OpenSubKey(Reg._key))
                    {
                        var left = regk.GetValue("Left");
                        var top = regk.GetValue("Top");

                        if (left != null && top != null)
                        {
                            point = new Point(Convert.ToInt16(left), Convert.ToInt16(top));

                            return true;
                        }
                    }
                }
            }
            catch
            {

            }

            point = Point.Empty;
            return false;
        }
    }
}